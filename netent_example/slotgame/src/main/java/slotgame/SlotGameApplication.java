package slotgame;

import framework.Constants;
import framework.GameApplication;
import framework.util.RandomNumberUtil;


/**
 * @author Krishan Babbar
 * 
 * This class will start Slot Game and follow the instructions on screen.
 * 
 * Game will have following features.
 * 
 * The player has an infinite amount of coins.
 * The player bets 10 coins to play a normal game round
 * In any round (free or normal), the player has a 30 % chance of winning back 20 coins.
 * In any round (free or normal), the player also has a 10% chance of triggering a free round 
 * where the player does not have to pay for bet. The free round works in the same way as a normal round 
 * except it costs 0 coins. The free round should follow immediately after the normal round.
 * The player can both win coins and free round at the same time.
 * 
 */
public class SlotGameApplication extends GameApplication {
	
	public SlotGameApplication() {
		init();		
	}	
	
	public static void main(String[] args) {		
		SlotGameApplication slotApp = new SlotGameApplication();	
		slotApp.startGame();
	}
	
	@Override
	protected boolean playRound(boolean isItFree) {
		
		int slotGameBetCost = this.getPerRoundBetCost(isItFree);
		
		if (!isItFree) {
			gameStats.setTotalCoinsBet(gameStats.getTotalCoinsBet() + slotGameBetCost);
		}
		else {
			gameStats.setNoOfFreeRounds(gameStats.getNoOfFreeRounds() + 1);
		}
		
		int roundNumber = RandomNumberUtil.getNextNumber();
		
		if (RandomNumberUtil.isWinningShot(roundNumber, Constants.SLOT_GAME_WINNING_PROBABILITY)) {
			gameStats.setTotalCoinsWon(gameStats.getTotalCoinsWon() + Constants.SLOT_GAME_WINNER_PRIZE);
			gameStats.setNoOfRoundsWon(gameStats.getNoOfRoundsWon() + 1);
			this.showWinningShot(roundNumber);			
		} 
		else {			
			gameStats.setNoOfRoundsLost(gameStats.getNoOfRoundsLost() + 1);
			this.showLostShot(roundNumber);
		}
		
		boolean isNextRoundFree = RandomNumberUtil.isWinningShot(roundNumber, Constants.SLOT_GAME_FREE_ROUND_PROBABILITY);
		if (isNextRoundFree) {
			System.out.println("Congratulations!! You have won a free round. Your next round bet amount will be " + 0 + " coins.");
		}
		
		return isNextRoundFree;		
	}
	
	private void showWinningShot(int luckyNumber) {
		System.out.println("Round Result: " + luckyNumber + " " + luckyNumber + " " + luckyNumber);
		System.out.println("Congratulations!! You have won this round. Your total winning amount: " + gameStats.getTotalCoinsWon());
	}
	
	private void showLostShot(int luckyNumber) {
		System.out.println("Round Result: " + luckyNumber * 2 + " " + luckyNumber * 3 + " " + luckyNumber * 4);
		System.out.println("You lost this round. Better luck next time. Try again.");
	}

	@Override
	protected int getPerRoundBetCost(boolean isNextRoundFree) {
		return SlotGamePrice.getPerRoundBetCost(isNextRoundFree);
	}
	
	@Override
	protected void showStats() {
		this.gameStats.showSlotGameStats();
	}
}
