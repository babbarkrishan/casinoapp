package slotgame;

import framework.Constants;

/**
 * @author Krishan Babbar
 *
 */
public class SlotGamePrice {

	public static int getPerRoundBetCost(boolean isNextRoundFree) {
		int roundBetCost = Constants.SLOT_GAME_COST;
		if (isNextRoundFree) {
			roundBetCost = 0;
		}
		
		return roundBetCost;
	}
}