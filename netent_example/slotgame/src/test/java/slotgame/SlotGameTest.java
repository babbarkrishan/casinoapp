package slotgame;

import org.junit.Test;
import static org.junit.Assert.*;
import framework.bean.GameStats;


/**
 * @author Krishan Babbar
 *
 */
public class SlotGameTest {
	@Test
	public void testICMPPing() {
		SlotGameApplication slotApp = new SlotGameApplication();	
		slotApp.setByPassUserOptions(true);
		slotApp.setNoOfRounds(100);
		slotApp.startGame();
		GameStats gameStats = slotApp.getGameStats();
		
		assertTrue (gameStats.getTotalCoinsWon() < gameStats.getTotalCoinsBet());
	}
}
