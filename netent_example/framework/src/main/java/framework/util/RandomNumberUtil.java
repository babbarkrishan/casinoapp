package framework.util;

import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;


/**
 * @author Krishan Babbar
 *
 */
public class RandomNumberUtil {
	private static Random random = new Random();
	
	public static int getNextNumber() {
		return random.nextInt(99);
	}

	public static boolean isWinningShot(int numberToCheck, int probabilityToCheck) {
		if (numberToCheck < probabilityToCheck) {
			return true;
		}
		return false;		
	}
	
	public static int getNextNumber(int seed) {
		return random.nextInt(seed);
	}
	
	public static long getNextNumberInRange(int minN, int maxN) {
		return ThreadLocalRandom.current().nextLong(1, 5);
	}
	
}
