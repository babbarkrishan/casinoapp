package framework.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import org.apache.commons.lang3.StringUtils;

import framework.Constants;


/**
 * @author Krishan Babbar
 *
 */
public class GameUtil {
	
	public static String askGameChoice(Scanner scanner, int roundBetCost, Map<String, String> gameOptions) {
		System.out.println("Cost for this round: " + roundBetCost + Constants.GAME_COST_UNIT);
		
		for (Map.Entry<String, String> entry : gameOptions.entrySet()) {
		    System.out.println(entry.getKey() + " - " + entry.getValue());
		}
		System.out.println("Choose your option from above list: ");
		
		while (true) {
			String gameChoice = scanner.nextLine().trim();
			if (!StringUtils.isEmpty(gameChoice) && isValidOption(gameChoice, gameOptions)) {
				return gameChoice;
			}
			else {
				System.out.println("Please enter valid option.");
			}
		}		
	}
	
	public static boolean isValidOption(String gameChoice, Map<String, String> gameOptions) {
		return gameOptions.containsKey((String)gameChoice);
	}
	
	public static Map<String, String> getGameOptions() {
		Map<String, String> gameOptions = new HashMap<String, String>();
		gameOptions.put(Constants.CONTINUE_ROUND_OPTION, "Continue Round");
		gameOptions.put(Constants.EXIT_GAME_SERVER_OPTION, "Exit Game");
		return gameOptions;
	}	
}
