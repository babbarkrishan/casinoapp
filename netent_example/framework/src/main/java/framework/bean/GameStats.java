package framework.bean;

import framework.Constants;


/**
 * @author Krishan Babbar
 *
 */
public class GameStats {
	
	private double totalCoinsBet;
	private double totalCoinsWon;
	private int noOfRoundsWon;
	private int noOfRoundsLost;
	private int noOfFreeRounds;
	private int noOfRounds;
	
	public GameStats() {
		totalCoinsBet = 0;
		totalCoinsWon = 0;
		noOfRoundsWon = 0;
		noOfRoundsLost = 0;
		noOfFreeRounds = 0;
		noOfRounds = Constants.NO_OF_ROUNDS;
	}
	
	public GameStats(double totalCoinsBet, double totalCoinsWon, int noOfRoundsWon, int noOfRoundsLost, 
			int noOfFreeRounds, int noOfRounds) {
		this.totalCoinsBet = totalCoinsBet;
		this.totalCoinsWon = totalCoinsWon;
		this.noOfRoundsWon = noOfRoundsWon;
		this.noOfRoundsLost = noOfRoundsLost;
		this.noOfFreeRounds = noOfFreeRounds;
		this.noOfRounds = noOfRounds;
	}
	
	public double getTotalCoinsBet() {
		return totalCoinsBet;
	}
	public void setTotalCoinsBet(double totalCoinsBet) {
		this.totalCoinsBet = totalCoinsBet;
	}
	public double getTotalCoinsWon() {
		return totalCoinsWon;
	}
	public void setTotalCoinsWon(double totalCoinsWon) {
		this.totalCoinsWon = totalCoinsWon;
	}
	public int getNoOfRoundsWon() {
		return noOfRoundsWon;
	}
	public void setNoOfRoundsWon(int noOfRoundsWon) {
		this.noOfRoundsWon = noOfRoundsWon;
	}
	public int getNoOfRoundsLost() {
		return noOfRoundsLost;
	}
	public void setNoOfRoundsLost(int noOfRoundsLost) {
		this.noOfRoundsLost = noOfRoundsLost;
	}
	public int getNoOfFreeRounds() {
		return noOfFreeRounds;
	}
	public void setNoOfFreeRounds(int noOfFreeRounds) {
		this.noOfFreeRounds = noOfFreeRounds;
	}
	public int getNoOfRounds() {
		return noOfRounds;
	}
	public void setNoOfRounds(int noOfRounds) {
		this.noOfRounds = noOfRounds;
	}
	
	public void showSlotGameStats() {
		System.out.println("###############	Thanks for playing Slot Game!!	###############");
		System.out.println();
		System.out.println("Today, you won: " + this.totalCoinsWon + " coins.");
		System.out.println("You bet: " + this.totalCoinsBet + " coins.");
		System.out.println("No of bets won: " + this.noOfRoundsWon);
		System.out.println("No of bets lost: " + this.noOfRoundsLost);
		System.out.println("No of free rounds: " + this.noOfFreeRounds);
		
		System.out.println();
		double rtp = this.totalCoinsWon/this.totalCoinsBet;
		System.out.println("RTP: " + rtp);
		System.out.println();
		System.out.println("###############	###############	###############");
		System.out.println();
		System.out.println("See you again!!");	
		
	}
	
	public void showBonusGameStats() {
		System.out.println("###############	Thanks for playing Bonus Game!!	###############");
		System.out.println();
		System.out.println("Today, you won: " + this.totalCoinsWon + " coins.");
		System.out.println("You bet: " + this.totalCoinsBet + " coins.");
		System.out.println("No of bets won: " + this.noOfRoundsWon);
		System.out.println("No of bets lost: " + this.noOfRoundsLost);
		
		System.out.println();
		double rtp = this.totalCoinsWon/this.totalCoinsBet;
		System.out.println("RTP: " + rtp);
		System.out.println();
		System.out.println("###############	###############	###############");
		System.out.println();
		System.out.println("See you again!!");	
		
	}
}
