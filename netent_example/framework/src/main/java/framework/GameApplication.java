package framework;

import java.util.Map;
import java.util.Scanner;

import framework.bean.GameStats;
import framework.util.GameUtil;


/**
 * @author Krishan Babbar
 *
 */
abstract public class GameApplication {
	
	protected Scanner scanner;
	protected GameStats gameStats;
	protected boolean byPassUserOptions;
	
	abstract protected boolean playRound(boolean isNextRoundFree);
	abstract protected int getPerRoundBetCost(boolean isNextRoundFree);
	abstract protected void showStats();
	
	protected void init() {
		scanner = new Scanner(System.in);
		gameStats = new GameStats();
		byPassUserOptions = Constants.BY_PASS_USER_CHOICE;
	}
	
	protected Map<String, String> getGameOptions() {
		return GameUtil.getGameOptions();
	}
	
	public void startGame() {
		System.out.println("###############	Welcome to the Game!!	###############");		
		
		boolean isNextRoundFree = false;		
		String gameChoice = Constants.CONTINUE_ROUND_OPTION;		
		int counter = 1;
		boolean isContinue = true;
		
		while(isContinue) {			
			if (this.isBypassUserChoice()) {
				if (counter >= gameStats.getNoOfRounds()) {
					isContinue = false;
				}
				counter++;
			}
			else {
				gameChoice = GameUtil.askGameChoice(scanner, this.getPerRoundBetCost(isNextRoundFree) , this.getGameOptions());
				if (gameChoice.equals(Constants.EXIT_GAME_SERVER_OPTION)) {
					isContinue = false;
				}
			}
			isNextRoundFree = this.playRound(isNextRoundFree);
		}
		
		this.showStats();
		this.releaseResources();		
	}
	
	
	private void releaseResources() {
		if (scanner != null) {
			scanner.close();
		}
	}
	
	public GameStats getGameStats() {
		return this.gameStats;
	}
	
	public void setByPassUserOptions(boolean byPassUserOptions) {
		this.byPassUserOptions = byPassUserOptions;		
	}
	
	public boolean isBypassUserChoice() {
		return this.byPassUserOptions;
	}
	
	public void setNoOfRounds(int noOfRounds) {
		gameStats.setNoOfRounds(noOfRounds);
	}
}
