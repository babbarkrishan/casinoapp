package framework;


/**
 * @author Krishan Babbar
 *
 */
public class Constants {
	public static final String EXIT_GAME_SERVER_OPTION = "9";
	public static final String CONTINUE_ROUND_OPTION = "1";
	
	public static final int SLOT_GAME_COST = 10;
	public static final int BONUS_GAME_COST = 10;
	public static final int SLOT_GAME_WINNER_PRIZE = 20;
	public static final int BOX_PICKUP_GAME_WINNER_PRIZE = 5;
	public static final String GAME_COST_UNIT = " coins";
	
	public static final int SLOT_GAME_WINNING_PROBABILITY = 30;
	public static final int SLOT_GAME_FREE_ROUND_PROBABILITY = 10;	
	public static final int BONUS_GAME_WINNING_PROBABILITY = 10;
	
	public static final int NO_OF_ROUNDS = 100;
	public static final boolean BY_PASS_USER_CHOICE = false;
}
