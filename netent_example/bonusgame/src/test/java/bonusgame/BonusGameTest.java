package bonusgame;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import bonusgame.BonusGameApplication;
import framework.bean.GameStats;

/**
 * @author Krishan Babbar
 *
 */
public class BonusGameTest {
	@Test
	public void testICMPPing() {
		BonusGameApplication bonusApp = new BonusGameApplication();
		bonusApp.setByPassUserOptions(true);
		bonusApp.setNoOfRounds(100);
		bonusApp.startGame();
		GameStats gameStats = bonusApp.getGameStats();
		
		assertTrue (gameStats.getTotalCoinsWon() < gameStats.getTotalCoinsBet());
	}
}
