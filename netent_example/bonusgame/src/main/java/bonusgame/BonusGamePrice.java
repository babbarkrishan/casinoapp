package bonusgame;

import framework.Constants;


/**
 * @author Krishan Babbar
 *
 */
public class BonusGamePrice {

	public static int getPerRoundBetCost() {
		return Constants.BONUS_GAME_COST;
	}
}