package bonusgame;

import framework.Constants;
import framework.GameApplication;
import framework.util.RandomNumberUtil;


/**
 * @author Krishan Babbar
 *
 * This class will start Bonus Game and follow the instructions on screen.
 * 
 * Game will have following features.
 * 
 * The player has an infinite amount of coins. 
 * The player bets 10 coins to play a normal game round.
 * In a normal game round, the player has a 10% chance of triggering a bonus round.
 * If the player wins a bonus round, they will play a separate �box picking game�. In this game the player is given 5 boxes. 
 * One of the boxes will end the box picking game, while the rest contain 5 coins each. The player is allowed to open boxes
 * until the game ends. The values should be assigned to the boxes randomly each time the player wins the bonus round.
 * The player should not be able to win coins by any other means.
 * 
 */
public class BonusGameApplication extends GameApplication {
	
	public BonusGameApplication() {
		init();
	}
	
	public static void main(String[] args) {
		BonusGameApplication bonusGameApp = new BonusGameApplication();
		bonusGameApp.startGame();
	}
	
	@Override
	protected boolean playRound(boolean isItFree) {
		int bonusGameBetCost= this.getPerRoundBetCost(isItFree);		
		gameStats.setTotalCoinsBet(gameStats.getTotalCoinsBet() + bonusGameBetCost);
		
		int roundNumber = RandomNumberUtil.getNextNumber();
		
		if (RandomNumberUtil.isWinningShot(roundNumber, Constants.BONUS_GAME_WINNING_PROBABILITY)) {
			BoxPickupGame boxPickupGame = new BoxPickupGame(this.isBypassUserChoice());
			int bonusRoundWinningAmount = boxPickupGame.playGame(scanner);
			gameStats.setTotalCoinsWon(gameStats.getTotalCoinsWon() + bonusRoundWinningAmount);
			gameStats.setNoOfRoundsWon(gameStats.getNoOfRoundsWon() + 1);
			this.showWinningShot(bonusRoundWinningAmount);
		} 
		else {			
			gameStats.setNoOfRoundsLost(gameStats.getNoOfRoundsLost() + 1);
			this.showLostShot(roundNumber);
		}
		
		return isItFree;		
	}
	
	private void showWinningShot(int bonusRoundWinningAmount) {
		System.out.println();
		System.out.println("###############	###############	###############");
		System.out.println();
		System.out.println("Amount won in this round: " + bonusRoundWinningAmount);
		System.out.println("Your total winning amount: " + gameStats.getTotalCoinsWon());
		System.out.println();
		System.out.println("###############	###############	###############");
		System.out.println();
	}
	
	private void showLostShot(int luckyNumber) {
		System.out.println("You lost this round. Better luck next time. Try again.");
	}

	@Override
	protected int getPerRoundBetCost(boolean isNextRoundFree) {
		return BonusGamePrice.getPerRoundBetCost();
	}
	
	@Override
	protected void showStats() {
		this.gameStats.showBonusGameStats();
	}
}
