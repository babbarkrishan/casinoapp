package bonusgame;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.apache.commons.lang3.StringUtils;

import framework.Constants;
import framework.util.RandomNumberUtil;


/**
 * @author Krishan Babbar
 *
 * This will start another game named �Box Picking Game�
 * It will be instantiated from Bonus Game.
 * 
 * If the player wins a bonus round in Bonus game, they will play a this �box picking game�. In this game the player is given 5 boxes. 
 * One of the boxes will end the box picking game, while the rest contain 5 coins each. The player is allowed to open boxes
 * until the game ends. The values should be assigned to the boxes randomly each time the player wins the bonus round.
 * 
 */
public class BoxPickupGame {

	private List<Integer> boxNumbers;
	private boolean byPassUserOptions;
	
	public BoxPickupGame(boolean byPassUserOptions) {
		this.byPassUserOptions = byPassUserOptions;
		initBoxNumbers();		
	}
	
	private void initBoxNumbers() {
		boxNumbers = new ArrayList<Integer>();
		for (int i = 1; i <= 5; i++) {
			boxNumbers.add(i);
		}		
	}

	private void showAvailableBoxNos() {
		for (Integer number: this.boxNumbers) {
			System.out.println(number);
		}
	}
	
	public int playGame(Scanner scanner) {
		System.out.println("Congratulations!! You won the bonus game i.e. Box Pickup Game!!");
		System.out.println("###############	Welcome to the Box Pickup Game!!	###############");
		
		int gameExitBoxNumber = (int) RandomNumberUtil.getNextNumberInRange(1, 5);
		//System.out.println("gameExitBoxNumber: " + gameExitBoxNumber);
		int boxGameWinningAmount = 0;
		boolean isContinue = true;
		String boxNumber;
		int bNo = 0;
		while (isContinue) {
			System.out.println("Below are given boxes number. One of the box will end this game. Each of the other box have 5 coins for you.");
			this.showAvailableBoxNos();			
			System.out.println("Choose your box number: ");
			
			if (this.byPassUserOptions) {
				boxNumber = String.valueOf(++bNo);				
			}
			else {
				boxNumber = scanner.nextLine().trim();
			}
			
			if (!StringUtils.isEmpty(boxNumber) && this.isValidOption(boxNumber)) {
				int boxNo = Integer.valueOf(boxNumber);
				
				if (boxNo != gameExitBoxNumber) {
					System.out.println("You won 5 coins!! Woooohhhooo!");
					boxGameWinningAmount += Constants.BOX_PICKUP_GAME_WINNER_PRIZE;
					int index = this.boxNumbers.indexOf(boxNo);
					this.boxNumbers.remove(index);
				}
				else {
					isContinue = false;
				}				
			}
			else {
				System.out.println("Please enter valid box number given above.");
			}
		}
		
		return boxGameWinningAmount;
	}

	private boolean isValidOption(String boxNumber) {
		try {
			int boxNo = Integer.valueOf(boxNumber);
			if (this.boxNumbers.contains(boxNo)) {
				return true;
			}
		}
		catch (NumberFormatException ex) {
			ex.printStackTrace();			
		}
		return false;
	}
	
}
