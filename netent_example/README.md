# README #

### What is this repository for? ###
This is a Maven based Java application having micro services. In this we are creating two simple games that share common code.

The first game (“slotgame”) should have the following rules:
• The player has an infinite amount of coins.
• The player bets 10 coins to play a normal game round
• In any round (free or normal), the player has a 30 % chance of winning back 20
coins.
• In any round (free or normal), the player also has a 10% chance of triggering a
free round where the player does not have to pay for bet. The free round works in
the same way as a normal round except it costs 0 coins. The free round should
follow immediately after the normal round.
• The player can both win coins and free round at the same time.

The second game (“bonusgame”) should have the following rules:
• The player has an infinite amount of coins.
• The player bets 10 coins to play a normal game round.
• In a normal game round, the player has a 10% chance of triggering a bonus round.
• If the player wins a bonus round, they will play a separate “box picking game”. In
this game the player is given 5 boxes. One of the boxes will end the box picking
game, while the rest contain 5 coins each. The player is allowed to open boxes
until the game ends. The values should be assigned to the boxes randomly each
time the player wins the bonus round.
• The player should not be able to win coins by any other means.

### How do I get set up? ###

* To setup the project in your eclipse follow the below steps.
	* Download the code
	* Import the project in Eclipse using existing Maven project option
	* Do maven clean and install to download all the required jars (maven dependencies)	
	* It will execute JUnit tests as well
	
* Dependencies
	* JDK 1.8
	* Maven
	* org.apache.commons

* Deployment instructions
	* There are two games.
	* To run a game you may run "BonusGameApplication" or "SlotGameApplication" as java application
	* Or export each game as a runnable jar and use following commands to run your games
		java -jar slotgame.jar
		java -jar bonusgame.jar
			

### Contribution guidelines ###

* Writing tests
	You may add your code to fix any bug (if any) or to extend the functionality.
	
* Code review
	Code should be up to the mark and there should not be any compilation error.
	Add instructions if required.
	

### Who do I talk to? ###

* You may reach to me at krishan.babbar@gmail.com
* GeeksQuest.com